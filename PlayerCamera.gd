extends Camera2D

var target

func _ready():
	target = get_tree().get_nodes_in_group("player")[0]

func _process(delta):
	position = target.position
