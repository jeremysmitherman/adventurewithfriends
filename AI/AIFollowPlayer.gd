extends Node

var mob
var targets = {}
var aggro_table = []
var target

func _ready():
	mob = get_owner()

func aggro_sort(a, b):
	if a[1] < b[1]:
		return true
	else:
		return false

func process_aggro_table():
	# Ensure all players are in target list
	var players = get_tree().get_nodes_in_group("players")
	for player in players:
		if not targets.has(player):
			targets[player] = 0
			print("Adding target")
	
	for target in targets:
		if not players.has(target):
			targets.erase(target)
			print("Removing target")
		
	# Rebuild aggro table
	aggro_table = []
	for player in targets:
		aggro_table.push_back([player, targets[player]])
	
	aggro_table.sort_custom(self, "aggro_sort")
	return aggro_table[0][0]

func _process(delta):
	#target = process_aggro_table()
		
	#if target:
	#	var direction = (target.position - mob.position).normalized() * 30
	#	mob.move_and_slide(direction)
	pass
