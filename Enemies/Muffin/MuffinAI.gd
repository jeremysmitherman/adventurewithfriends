extends "res://AI/AIFollowPlayer.gd"

var move_timer = 0
var move_time = 60 + randf() * 41 + 1
var move_target = null
var travel_time = 0
var max_travel_time = 20
var speed = 60

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	mob = get_owner()

func _process(delta):
	target = .process_aggro_table()
	move_timer += 1
	if move_timer > move_time and not move_target:
		move_target = target.position

	if move_target:
		var direction = (move_target - mob.position).normalized()
		mob.move_and_slide(direction * speed)
		travel_time += 1
	
	if travel_time > max_travel_time:
		move_target = null
		travel_time = 0
		move_timer = 0
		var move_time = 60 + randf() * 41 + 1
		
		
	
	
