extends Node

signal knockback_finished

var mob
var original_collision_layer
var original_collision_mask
var is_active = false
var knockback_direction
var knockback_speed = 120
var knockback_frames = 10
var cur_knockback_frames = 0

func _ready():
	mob = get_owner()
	original_collision_layer = mob.collision_layer
	original_collision_mask = mob.collision_mask
	print(original_collision_mask)
	

func do_knockback(direction):
	print("Doing knockback")
	mob.set_collision_mask_bit(2, false)
	is_active = true
	knockback_direction = direction
	cur_knockback_frames = 0
	

func _physics_process(delta):
	if is_active and cur_knockback_frames <= knockback_frames:
		mob.move_and_slide(knockback_direction * (knockback_speed))
		cur_knockback_frames += 1
	elif is_active and cur_knockback_frames >= knockback_frames:
		is_active = false
		emit_signal("knockback_finished")
