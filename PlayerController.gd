extends Node

var mob
var melee_area
var melee_anim
var melee_hit_enemies = {}

var inputs = Vector2()
var previous_inputs = Vector2()
var attack_charge = 0
var is_attacking = false
var cur_attack_str = ATTACK_STRENGTH.WEAK

enum ATTACK_STRENGTH {WEAK, MID, STRONG}

func _ready():
	mob = get_node("..")
	melee_anim = get_node("../AnimationPlayer")
	melee_area = get_node("../Area2D")


func _process(delta):
	get_attack_state(delta)
	if melee_area.monitoring:
		var bodies = melee_area.get_overlapping_bodies()
		for body in bodies:
			if body.is_in_group('enemies') and !melee_hit_enemies.has(body):
				melee_hit_enemies[body] = 1
				body.take_damage(body.damage_type.PHYSICAL, 10, get_node('../Area2D'))
				body.cur_state = body.state.KNOCKBACK
				print("Hit " + body.get_name() +  " with a " + str(cur_attack_str) + " attack!")
	inputs = get_movement_input()


func _physics_process(delta):
	# If we're charing an attack, slow move speed
	var speed = mob.move_speed
	if attack_charge != 0 or is_attacking:
		speed = speed / 2
	else:
		speed = mob.move_speed
		
	# Adjust facing
	if attack_charge == 0 and not is_attacking:
		if inputs.x == 1:
			mob.cur_facing = mob.facing.RIGHT
		elif inputs.x == -1:
			mob.cur_facing = mob.facing.LEFT
		elif inputs.y == 1:
			mob.cur_facing = mob.facing.DOWN
		elif inputs.y == -1:
			mob.cur_facing = mob.facing.UP
	
	match mob.cur_facing:
		mob.facing.DOWN:
			melee_area.transform[2] = Vector2(0.0, 16.0)
		mob.facing.UP:
			melee_area.transform[2] = Vector2(0.0, -16.0)
		mob.facing.RIGHT:
			melee_area.transform[2] = Vector2(16.0, 0.0)
		mob.facing.LEFT:
			melee_area.transform[2] = Vector2(-16.0, 0.0)
	
	var motion = mob.move_and_slide(inputs.normalized() * speed)


func get_movement_input():
	inputs.x = 0
	inputs.y = 0
	var right = Input.is_action_pressed("ui_right")
	var left = Input.is_action_pressed("ui_left")
	var up = Input.is_action_pressed("ui_up")
	var down = Input.is_action_pressed("ui_down")
	
	if right:
		inputs.x += 1
	if left:
		inputs.x -= 1
	if up:
		inputs.y -= 1
	if down:
		inputs.y += 1
	
	return inputs
	

func get_attack_state(delta):
	if Input.is_action_pressed("melee_attack") and not is_attacking:
		attack_charge += 1 * delta
	
	if Input.is_action_just_released("melee_attack") and not is_attacking:
		if attack_charge < .25:
			cur_attack_str = ATTACK_STRENGTH.WEAK
			melee_anim.play("atk_weak")
		if attack_charge > .3 and attack_charge < .8:
			cur_attack_str = ATTACK_STRENGTH.MID
			melee_anim.play("atk_weak")
		if attack_charge > .8:
			cur_attack_str = ATTACK_STRENGTH.STRONG
			melee_anim.play("atk_weak")
		is_attacking = true
		attack_charge = 0


func _on_AnimationPlayer_animation_finished(anim_name):
	if "atk" in anim_name:
		melee_hit_enemies = {}
		is_attacking = false
