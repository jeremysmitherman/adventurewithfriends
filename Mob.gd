extends KinematicBody2D

enum facing {UP,LEFT,RIGHT,DOWN}
enum damage_type {PHYSICAL}
enum state {NORMAL, KNOCKBACK}

var move_speed = 60
var hp = 100
var stm = 100
var mp = 5
var cur_facing = facing.DOWN
var cur_state = state.NORMAL
var knockback_node

func _ready():
	knockback_node = get_node("Knockback")
	if knockback_node:
		knockback_node.connect("knockback_finished", self, "_on_knockback_finished")

func take_damage(type, amount, initiator):
	hp -= amount
	print(initiator.get_owner())
	if knockback_node and cur_state == state.NORMAL:
		var direction = (global_position - initiator.global_position).normalized()
		cur_state = state.KNOCKBACK
		knockback_node.do_knockback(direction)

func _on_knockback_finished():
	print("Knockback finished")
	cur_state = state.NORMAL

func _physics_process(delta):
	if hp <= 0:
		queue_free()

func _process(delta):
	if is_in_group("enemies"):
    	pass #print(cur_state)
